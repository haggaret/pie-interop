package com.magor.pie.interop;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@ClientEndpoint
public class WebsocketClientEndpoint
{
    private static Logger log = LoggerFactory.getLogger(WebsocketClientEndpoint.class);

    protected WebSocketContainer container;
    Session userSession = null;
    private int currentId;
    private MessageHandler defaultMessageHandler;
    private Map<String, MessageHandler> callbacks;

    public WebsocketClientEndpoint()
    {
        container = ContainerProvider.getWebSocketContainer();
        callbacks = new HashMap<String, MessageHandler>();
        currentId = 1;
    }

    public String serializeJsonRpcMsg(JsonRpcMessage message) throws JsonProcessingException
    {
        log.debug("Attempting to serialize: " + message.toString());
        ObjectMapper mapper = new ObjectMapper();
        String msgAsString = mapper.writeValueAsString(message);
        return msgAsString;
    }

    /**
     * Connect to server
     *
     * @param uri for the server to connect to
     */
    public void connect(String uri) throws URISyntaxException
    {
        try
        {
            container.connectToServer(this, URI.create(uri));
        }
        catch (DeploymentException | IOException e)
        {
            log.error(e.toString());
        }
    }

    /**
     * Disconnect from server
     *
     */
    public void disconnect() throws IOException
    {
        userSession.close();
    }

    /**
     * Callback hook for Connection open events.
     *
     * @param userSession the userSession which is opened.
     */
    @OnOpen
    public void onOpen(Session userSession) throws Exception
    {
        // This is like onWSConnect or onWSLogin
        log.debug("Websocket Connected");
        this.userSession = userSession;
    }

    /**
     * Callback hook for Connection close events.
     *
     * @param userSession the userSession which is getting closed.
     * @param reason the reason for connection close
     */
    @OnClose
    public void onClose(Session userSession, CloseReason reason)
    {
        // This is like onWSClose
        log.debug("closing websocket because of " + reason);
        this.userSession = null;
    }


    /**
     * Callback hook for Message Events. This method will be invoked when a client receives a message.
     *
     * @param message The message as a String
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    @OnMessage
    public void onMessage(Session userSession, String message) throws JsonParseException, JsonMappingException, IOException
    {
        // This is like onmessage
        log.debug("Received message: " + message);

        ObjectMapper mapper = new ObjectMapper();
        JsonRpcMessage rpcMsg = null;
        try
        {
            rpcMsg = mapper.readValue(message, JsonRpcMessage.class);
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (rpcMsg != null && callbacks.containsKey(String.valueOf(rpcMsg.getId())))
        {
            MessageHandler callback = callbacks.get(String.valueOf(rpcMsg.getId()));
            callbacks.remove(String.valueOf(rpcMsg.getId()));
            callback.handleMessage(rpcMsg);
        }

        if (this.defaultMessageHandler != null)
        {
            this.defaultMessageHandler.handleMessage(rpcMsg);
        }
    }

    /**
     * Send a message.
     *
     * @param message
     */
    public void sendMessage(String method, JsonRpcMsgParams params, MessageHandler msgHandler) throws IOException
    {
        log.debug("Received a " + method + " message to send");
        JsonRpcMessage message = new JsonRpcMessage();
        message.setMethod(method);
        message.setId(currentId++);
        message.setParams(params);

        String msgAsString = serializeJsonRpcMsg(message);

        log.debug("Sending message: " + msgAsString);
        userSession.getAsyncRemote().sendText(msgAsString);
        if (msgHandler != null)
        {
            callbacks.put(String.valueOf(currentId), msgHandler);
        }
    }

    /**
     * Send a message.
     *
     * @param message
     */
    public void sendResultMessage(String method, int id, MessageHandler msgHandler) throws IOException
    {
        log.debug("Received a " + method + " message to send");
        JsonRpcMessage message = new JsonRpcMessage();
        JsonRpcMessage.Result result = new JsonRpcMessage.Result();
        result.setMethod(method);
        message.setResult(result);
        message.setId(id);

        String msgAsString = serializeJsonRpcMsg(message);

        log.debug("Sending message: " + msgAsString);
        userSession.getAsyncRemote().sendText(msgAsString);
        if (msgHandler != null)
        {
            callbacks.put(String.valueOf(currentId), msgHandler);
        }
    }

    /**
     * register message handler
     *
     * @param msgHandler
     */
    public void addDefaultMessageHandler(MessageHandler msgHandler)
    {
        log.debug("Adding message handler for websocket client");
        this.defaultMessageHandler = msgHandler;
    }

    /**
     * Message handler.
     *
     */
    public interface MessageHandler
    {
        public void handleMessage(JsonRpcMessage message);
    }
}
