package com.magor.pie.interop;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

//@JsonIgnoreProperties(ignoreUnknown = true)
public class Event implements Serializable
{
    private static final long serialVersionUID = 1L;

    public static class PluginData implements Serializable
    {
        private static final long serialVersionUID = 1L;

        public static class Data implements Serializable
        {
            private static final long serialVersionUID = 1L;

            private PluginDataResult result;
            private String sip;

            public Data()
            {

            }

            public PluginDataResult getResult()
            {
                return result;
            }

            public void setResult(PluginDataResult result)
            {
                this.result = result;
            }

            public String getSip()
            {
                return sip;
            }

            public void setSip(String sip)
            {
                this.sip = sip;
            }

            @Override
            public String toString()
            {
                return String.format("Data [result=%s, sip=%s]", result, sip);
            }
        }

        private Data data;
        private String plugin;

        public PluginData()
        {

        }

        public Data getData()
        {
            return data;
        }

        public void setData(Data data)
        {
            this.data = data;
        }

        public String getPlugin()
        {
            return plugin;
        }

        public void setPlugin(String plugin)
        {
            this.plugin = plugin;
        }

        @Override
        public String toString()
        {
            return String.format("PluginData [data=%s, plugin=%s]", data, plugin);
        }
    }

    public static class Jsep implements Serializable
    {
        private static final long serialVersionUID = 1L;

        private String type;
        private String sdp;

        public Jsep()
        {

        }

        public String getType()
        {
            return type;
        }

        public void setType(String type)
        {
            this.type = type;
        }

        public String getSdp()
        {
            return sdp;
        }

        public void setSdp(String sdp)
        {
            this.sdp = sdp;
        }

        @Override
        public String toString()
        {
            return String.format("Jsep [type=%s, sdp=%s]", type, sdp);
        }
    }

    private PluginData plugindata;
    private String janus;
    @JsonProperty("session_id")
    private long sessionId;
    private long sender;
    private String transaction;
    private Jsep jsep;

    public Event()
    {

    }

    public PluginData getPlugindata()
    {
        return plugindata;
    }

    public void setPlugindata(PluginData plugindata)
    {
        this.plugindata = plugindata;
    }

    public String getJanus()
    {
        return janus;
    }

    public void setJanus(String janus)
    {
        this.janus = janus;
    }

    public long getSessionId()
    {
        return sessionId;
    }

    public void setSessionId(long sessionId)
    {
        this.sessionId = sessionId;
    }

    public long getSender()
    {
        return sender;
    }

    public void setSender(long sender)
    {
        this.sender = sender;
    }

    public String getTransaction()
    {
        return transaction;
    }

    public void setTransaction(String transaction)
    {
        this.transaction = transaction;
    }

    public Jsep getJsep()
    {
        return jsep;
    }

    public void setJsep(Jsep jsep)
    {
        this.jsep = jsep;
    }

    @Override
    public String toString()
    {
        return String.format("Event [plugindata=%s, janus=%s, sessionId=%s, sender=%s, transaction=%s, jsep=%s]", plugindata, janus,
                             sessionId, sender, transaction, jsep);
    }
}
