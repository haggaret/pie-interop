package com.magor.pie.interop;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class DialogParams implements Serializable
{
    private static final long serialVersionUID = 1L;
    private Boolean useVideo;
    private Boolean useStereo;
    private Boolean screenShare;
    private String useMic;
    private String tag;
    String useSpeak;
    String localTag;
    String login;
    private String callID;
    @JsonProperty("caller_id_name")
    private String callerIdName;
    @JsonProperty("caller_id_number")
    private String callerIdNumber;
    @JsonProperty("display_direction")
    private String displayDirection;
    private Boolean wantVideo;
    @JsonProperty("remote_caller_id_name")
    private String remoteCallerIdName;
    @JsonProperty("remote_caller_id_number")
    private String remoteCallerIdNumber;
    @JsonProperty("destination_number")
    private String destinationNumber;
    private VideoParams videoParams;

    public Boolean getUseVideo()
    {
        return useVideo;
    }

    public void setUseVideo(Boolean useVideo)
    {
        this.useVideo = useVideo;
    }

    public Boolean getUseStereo()
    {
        return useStereo;
    }

    public void setUseStereo(Boolean useStereo)
    {
        this.useStereo = useStereo;
    }

    public Boolean getScreenShare()
    {
        return screenShare;
    }

    public void setScreenShare(Boolean screenShare)
    {
        this.screenShare = screenShare;
    }

    public String getUseMic()
    {
        return useMic;
    }

    public void setUseMic(String useMic)
    {
        this.useMic = useMic;
    }

    public String getTag()
    {
        return tag;
    }

    public void setTag(String tag)
    {
        this.tag = tag;
    }

    public String getUseSpeak()
    {
        return useSpeak;
    }

    public void setUseSpeak(String useSpeak)
    {
        this.useSpeak = useSpeak;
    }

    public String getLocalTag()
    {
        return localTag;
    }

    public void setLocalTag(String localTag)
    {
        this.localTag = localTag;
    }

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getCallID()
    {
        return callID;
    }

    public void setCallID(String callID)
    {
        this.callID = callID;
    }

    public String getCallerIdName()
    {
        return callerIdName;
    }

    public void setCallerIdName(String callerIdName)
    {
        this.callerIdName = callerIdName;
    }

    public String getCallerIdNumber()
    {
        return callerIdNumber;
    }

    public void setCallerIdNumber(String callerIdNumber)
    {
        this.callerIdNumber = callerIdNumber;
    }

    public String getDisplayDirection()
    {
        return displayDirection;
    }

    public void setDisplayDirection(String displayDirection)
    {
        this.displayDirection = displayDirection;
    }

    public Boolean getWantVideo()
    {
        return wantVideo;
    }

    public void setWantVideo(Boolean wantVideo)
    {
        this.wantVideo = wantVideo;
    }

    public String getRemoteCallerIdName()
    {
        return remoteCallerIdName;
    }

    public void setRemoteCallerIdName(String remoteCallerIdName)
    {
        this.remoteCallerIdName = remoteCallerIdName;
    }

    public String getRemoteCallerIdNumber()
    {
        return remoteCallerIdNumber;
    }

    public void setRemoteCallerIdNumber(String remoteCallerIdNumber)
    {
        this.remoteCallerIdNumber = remoteCallerIdNumber;
    }

    public String getDestinationNumber()
    {
        return destinationNumber;
    }

    public void setDestinationNumber(String destinationNumber)
    {
        this.destinationNumber = destinationNumber;
    }

    public VideoParams getVideoParams()
    {
        return videoParams;
    }

    public void setVideoParams(VideoParams videoParams)
    {
        this.videoParams = videoParams;
    }

    @Override
    public String toString()
    {
        return String.format("DialogParams [useVideo=%s, useStereo=%s, screenShare=%s, useMic=%s, tag=%s, useSpeak=%s, localTag=%s, login=%s, callID=%s, callerIdName=%s, callerIdNumber=%s, displayDirection=%s, wantVideo=%s, remoteCallerIdName=%s, remoteCallerIdNumber=%s, destinationNumber=%s, videoParams=%s]",
                             useVideo, useStereo, screenShare, useMic, tag, useSpeak, localTag, login, callID, callerIdName, callerIdNumber,
                             displayDirection, wantVideo, remoteCallerIdName, remoteCallerIdNumber, destinationNumber, videoParams);
    }
}
