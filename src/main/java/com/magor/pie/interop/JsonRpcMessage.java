package com.magor.pie.interop;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class JsonRpcMessage implements Serializable
{
    @JsonInclude(Include.NON_NULL)
    public static class Error implements Serializable
    {
        private static final long serialVersionUID = 1L;

        private int code;
        private String message;
        private String callID;

        public Error()
        {

        }

        public int getCode()
        {
            return code;
        }

        public void setCode(int code)
        {
            this.code = code;
        }

        public String getMessage()
        {
            return message;
        }

        public void setMessage(String message)
        {
            this.message = message;
        }

        public String getCallID()
        {
            return callID;
        }

        public void setCallID(String callID)
        {
            this.callID = callID;
        }

        @Override
        public String toString()
        {
            return "Error [code=" + code + ", message=" + message + ", callID=" + callID + "]";
        }
    }

    @JsonInclude(Include.NON_NULL)
    public static class Result implements Serializable
    {
        private static final long serialVersionUID = 1L;

        private String method;
        private String message;
        private int causeCode;
        private String cause;
        private String sessid;
        private String callID;

        public Result()
        {

        }

        public String getMethod()
        {
            return method;
        }

        public void setMethod(String method)
        {
            this.method = method;
        }

        public String getMessage()
        {
            return message;
        }

        public void setMessage(String message)
        {
            this.message = message;
        }

        public int getCauseCode()
        {
            return causeCode;
        }

        public void setCauseCode(int causeCode)
        {
            this.causeCode = causeCode;
        }

        public String getCause()
        {
            return cause;
        }

        public void setCause(String cause)
        {
            this.cause = cause;
        }

        public String getSessid()
        {
            return sessid;
        }

        public void setSessid(String sessid)
        {
            this.sessid = sessid;
        }

        public String getCallID()
        {
            return callID;
        }

        public void setCallID(String callID)
        {
            this.callID = callID;
        }

        @Override
        public String toString()
        {
            return String.format("Result [method=%s, message=%s, causeCode=%s, cause=%s, sessid=%s, callID=%s]", method, message, causeCode,
                                 cause, sessid, callID);
        }
    }

    private static final long serialVersionUID = 1L;
    private String jsonrpc = "2.0";
    private String method;
    private JsonRpcMsgParams params;
    private Error error;
    private Result result;
    private int id;

    public JsonRpcMessage()
    {

    }

    public String getJsonrpc()
    {
        return jsonrpc;
    }

    public void setJsonrpc(String jsonrpc)
    {
        this.jsonrpc = jsonrpc;
    }

    public String getMethod()
    {
        return method;
    }

    public void setMethod(String method)
    {
        this.method = method;
    }

    public Result getResult()
    {
        return result;
    }

    public void setResult(Result result)
    {
        this.result = result;
    }

    public JsonRpcMsgParams getParams()
    {
        return params;
    }

    public void setParams(JsonRpcMsgParams params)
    {
        this.params = params;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public Error getError()
    {
        return error;
    }

    public void setError(Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return String.format("JsonRpcMessage [jsonrpc=%s, method=%s, params=%s, error=%s, result=%s, id=%s]", jsonrpc, method, params,
                             error, result, id);
    }
}
