package com.magor.pie.interop;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magor.rest.client.RestClient;
import com.magor.rest.client.exception.RestException;

public class InteropRest
{
    private static Logger log = LoggerFactory.getLogger(InteropRest.class);
    private static RestClient restClient = new RestClient("http://192.168.216.122:8088/janus", 30000, 45000);

    public InteropRest()
    {

    }

    private long setupSession()
    {
        long sessionId = 0;
        log.info("Starting Session");
        Map<String, String> body = new HashMap<String, String>();
        body.put("janus", "create");
        body.put("transaction", "123456789abc");
        try
        {
            SessionIdResponse sessionIdResponse = restClient.post("", body, SessionIdResponse.class);
            log.info(sessionIdResponse.toString());
            sessionId = sessionIdResponse.getData().getId();
        }
        catch (RestException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return sessionId;
    }

    private long attachSipPlugin(long sessionId)
    {
        long pluginId = 0;
        log.info("Attaching to SIP plugin");
        Map<String, String> body = new HashMap<String, String>();
        body.put("janus", "attach");
        body.put("plugin", "janus.plugin.sip");
        body.put("transaction", "123456789abd");
        try
        {
            PluginHandleResponse pluginHandleResponse = restClient.post(String.valueOf(sessionId), body, PluginHandleResponse.class);
            log.info(pluginHandleResponse.toString());
            pluginId = pluginHandleResponse.getData().getId();
        }
        catch (RestException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return pluginId;
    }

    private RegisterResponse registerSipUser(long sessionId, long pluginId, String server, String user, String passwd, String displayName)
    {
        log.info("Attempting to register user " + user + " with server at " + server);
        String proxy = "sip:" + server + ":5060";
        String username = "sip:" + user + "@" + server;
        Map<String, String> body = new HashMap<String, String>();
        body.put("request", "register");
        body.put("username", username);
        body.put("display_name", displayName);
        body.put("secret", passwd);
        body.put("proxy", proxy);
        Map<String, Object> registration_info = new HashMap<String, Object>();
        registration_info.put("janus", "message");
        registration_info.put("body", body);
        registration_info.put("transaction", "123456789abe");
        String url = String.valueOf(sessionId) + "/" + String.valueOf(pluginId);
        try
        {
            RegisterResponse registerResponse = restClient.post(url, registration_info, RegisterResponse.class);
            log.info(registerResponse.toString());
            return registerResponse;
        }
        catch (RestException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    private boolean handleEvent(long sessionId)
    {
        boolean response = false;
        String url = String.valueOf(sessionId);
        Map<String, String> queryParams = new HashMap<String, String>();
        queryParams.put("maxev", "1");
        try
        {
            Event event = restClient.get(url, queryParams, MediaType.APPLICATION_JSON_TYPE, Event.class);
            log.debug(event.toString());
            if (event.getJanus().equalsIgnoreCase("keepalive"))
            {
                // Do nothing - just a keep alive
                log.info("Keepalive...");
                response = true;
            }
            else if (event.getJanus().equalsIgnoreCase("event"))
            {
                log.info("Received an event");
                if (event.getPlugindata() != null && event.getPlugindata().getPlugin() != null
                    && event.getPlugindata().getPlugin().equalsIgnoreCase("janus.plugin.sip"))
                {
                    PluginDataResult result = event.getPlugindata().getData().getResult();
                    if ("registering".equalsIgnoreCase(result.getEvent()))
                    {
                        log.info(" Registering...");
                        response = true;
                    }
                    else if ("registered".equalsIgnoreCase(result.getEvent()))
                    {
                        log.info(" Successfully registered as " + result.getUsername());
                        response = true;
                    }
                    else if ("incomingcall".equalsIgnoreCase(result.getEvent()))
                    {
                        log.info(" Incoming call from " + result.getDisplayname());
                        log.info(" SDP: \n" + event.getJsep().getSdp());
                        response = true;
                    }
                }
            }
        }
        catch (RestException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return response;
    }

    public static void main(String[] args)
    {
        InteropRest t = new InteropRest();
        long sessionId = 0;
        long pluginId = 0;

        SessionIdResponse test = new SessionIdResponse();
        SessionIdResponse.Data data = new SessionIdResponse.Data();

        // Set up the session
        sessionId = t.setupSession();
        log.info("Session ID: " + sessionId);

        // Attach to the SIP plugin
        pluginId = t.attachSipPlugin(sessionId);
        if (pluginId != 0)
        {
            log.info("SIP plugin attached - plugin ID: " + pluginId);
        }

        // Register with SIP Registrar
        RegisterResponse response = t.registerSipUser(sessionId, pluginId, "192.168.216.206", "test", "testpasswd", "Test");
        if (response != null)
        {
            if (!response.getJanus().equalsIgnoreCase("ack"))
            {
                log.info("Registration Failed: " + response.getJanus());
            }
        }

        while (true)
        {
            t.handleEvent(sessionId);
        }
    }
}
