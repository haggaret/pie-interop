package com.magor.pie.interop;

import java.io.Serializable;

public class VideoParams implements Serializable
{
    private static final long serialVersionUID = 1L;

    private String minWidth;
    private String minHeight;
    private int minFrameRate;

    public VideoParams()
    {

    }

    public String getMinWidth()
    {
        return minWidth;
    }

    public void setMinWidth(String minWidth)
    {
        this.minWidth = minWidth;
    }

    public String getMinHeight()
    {
        return minHeight;
    }

    public void setMinHeight(String minHeight)
    {
        this.minHeight = minHeight;
    }

    public int getMinFrameRate()
    {
        return minFrameRate;
    }

    public void setMinFrameRate(int minFrameRate)
    {
        this.minFrameRate = minFrameRate;
    }

    @Override
    public String toString()
    {
        return "VideoParams [minWidth=" + minWidth + ", minHeight=" + minHeight + ", minFrameRate=" + minFrameRate + "]";
    }
}
