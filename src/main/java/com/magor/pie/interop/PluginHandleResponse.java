package com.magor.pie.interop;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PluginHandleResponse extends SessionIdResponse
{
    private static final long serialVersionUID = 1L;
    @JsonProperty("session_id")
    private long sessionId;

    public PluginHandleResponse()
    {

    }

    public long getSessionId()
    {
        return sessionId;
    }

    public void setSessionId(long sessionId)
    {
        this.sessionId = sessionId;
    }
}
