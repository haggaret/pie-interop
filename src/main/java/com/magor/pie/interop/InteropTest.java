package com.magor.pie.interop;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InteropTest
{
    private static Logger log = LoggerFactory.getLogger(InteropTest.class);
    private static boolean loggedIn = false;
    private static String incomingSdp = null;
    private static String outgoingSdp = null;
    private static JsonRpcMsgParams incomingParams = null;
    private static JsonRpcMsgParams outgoingParams = null;

    public static void handleInteropMessage(InteropEndpoint.MsgType msgType, JsonRpcMessage message)
    {
        switch (msgType)
        {
            case LOGGED_IN:
                log.info("Logged in");
                loggedIn = true;
                break;
            case IDLE:
                log.info("System is Idle");
                break;
            case INCOMING_CALL:
                log.info("Incoming Call");
                incomingSdp = message.getParams().getSdp();
                log.info("SDP:\n" + incomingSdp);
                incomingParams = message.getParams();
                break;
            case CONNECTED:
                log.info("System is Connected");
                break;
            case OUTGOING_CALL:
                log.info("Outgoing Call");
                outgoingSdp = message.getParams().getSdp();
                log.info("SDP:\n" + incomingSdp);
                outgoingParams = message.getParams();
                break;
            case CANCEL:
                log.info("Call Canceled");
                incomingSdp = null;
                incomingParams = null;
                break;
            case CALL_ENDED:
                log.info("Call Ended");
                incomingSdp = null;
                incomingParams = null;
                break;
            case ERROR:
                log.info("ERROR!");
                break;
        }
    }

    public static void main(String[] args)
    {

        InteropEndpoint interop = new InteropEndpoint("ws://verto.magor.local:8081");
        // add listener
        interop.addMessageHandler(new InteropEndpoint.MessageHandler()
        {
            @Override
            public void handleMessage(InteropEndpoint.MsgType msgType, JsonRpcMessage message)
            {
                // TODO Auto-generated method stub
                handleInteropMessage(msgType, message);
            }
        });

        while (!loggedIn)
        {
            // Wait to get logged in
            try
            {
                Thread.sleep(1000);
            }
            catch (InterruptedException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        String sdp = "v=0\r\no=- 4310699400773618292 2 IN IP4 127.0.0.1\r\ns=-\r\nt=0 0\r\na=group:BUNDLE audio video\r\na=msid-semantic: WMS UuELRAP9osB2u2DSvQTl1a6AlL5XRcpV78yV\r\nm=audio 9 UDP/TLS/RTP/SAVPF 111 103 104 9 0 8 106 105 13 126\r\nc=IN IP4 0.0.0.0\r\na=rtcp:9 IN IP4 0.0.0.0\r\na=ice-ufrag:R/jNioT1KMU+jcaN\r\na=ice-pwd:8R5CB3UoOXOVHpz7UefSXxyF\r\na=fingerprint:sha-256 91:D8:CD:CD:11:E6:69:9E:A9:66:FD:26:97:E5:24:AD:38:65:BD:C0:4F:C5:5D:21:2A:97:DE:DE:C7:A5:36:D0\r\na=setup:actpass\r\na=mid:audio\r\na=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level\r\na=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time\r\na=recvonly\r\na=rtcp-mux\r\na=rtpmap:111 opus/48000/2\r\na=rtcp-fb:111 transport-cc\r\na=fmtp:111 minptime=10;useinbandfec=1\r\na=rtpmap:103 ISAC/16000\r\na=rtpmap:104 ISAC/32000\r\na=rtpmap:9 G722/8000\r\na=rtpmap:0 PCMU/8000\r\na=rtpmap:8 PCMA/8000\r\na=rtpmap:106 CN/32000\r\na=rtpmap:105 CN/16000\r\na=rtpmap:13 CN/8000\r\na=rtpmap:126 telephone-event/8000\r\na=maxptime:60\r\nm=video 9 UDP/TLS/RTP/SAVPF 100 101 107 116 117 96 97 99 98\r\nc=IN IP4 0.0.0.0\r\na=rtcp:9 IN IP4 0.0.0.0\r\na=ice-ufrag:R/jNioT1KMU+jcaN\r\na=ice-pwd:8R5CB3UoOXOVHpz7UefSXxyF\r\na=fingerprint:sha-256 91:D8:CD:CD:11:E6:69:9E:A9:66:FD:26:97:E5:24:AD:38:65:BD:C0:4F:C5:5D:21:2A:97:DE:DE:C7:A5:36:D0\r\na=setup:actpass\r\na=mid:video\r\na=extmap:2 urn:ietf:params:rtp-hdrext:toffset\r\na=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time\r\na=extmap:4 urn:3gpp:video-orientation\r\na=sendrecv\r\na=rtcp-mux\r\na=rtcp-rsize\r\na=rtpmap:100 VP8/90000\r\na=rtcp-fb:100 ccm fir\r\na=rtcp-fb:100 nack\r\na=rtcp-fb:100 nack pli\r\na=rtcp-fb:100 goog-remb\r\na=rtcp-fb:100 transport-cc\r\na=rtpmap:101 VP9/90000\r\na=rtcp-fb:101 ccm fir\r\na=rtcp-fb:101 nack\r\na=rtcp-fb:101 nack pli\r\na=rtcp-fb:101 goog-remb\r\na=rtcp-fb:101 transport-cc\r\na=rtpmap:107 H264/90000\r\na=rtcp-fb:107 ccm fir\r\na=rtcp-fb:107 nack\r\na=rtcp-fb:107 nack pli\r\na=rtcp-fb:107 goog-remb\r\na=rtcp-fb:107 transport-cc\r\na=fmtp:107 level-asymmetry-allowed=1;packetization-mode=1;profile-level-id=42e01f\r\na=rtpmap:116 red/90000\r\na=rtpmap:117 ulpfec/90000\r\na=rtpmap:96 rtx/90000\r\na=fmtp:96 apt=100\r\na=rtpmap:97 rtx/90000\r\na=fmtp:97 apt=101\r\na=rtpmap:99 rtx/90000\r\na=fmtp:99 apt=107\r\na=rtpmap:98 rtx/90000\r\na=fmtp:98 apt=116\r\na=ssrc-group:FID 363608526 1231573672\r\na=ssrc:363608526 cname:ci/XCOZh63nQ/y3F\r\na=ssrc:363608526 msid:UuELRAP9osB2u2DSvQTl1a6AlL5XRcpV78yV 6aae665a-d1f6-4691-8f94-d0e3bcc72d59\r\na=ssrc:363608526 mslabel:UuELRAP9osB2u2DSvQTl1a6AlL5XRcpV78yV\r\na=ssrc:363608526 label:6aae665a-d1f6-4691-8f94-d0e3bcc72d59\r\na=ssrc:1231573672 cname:ci/XCOZh63nQ/y3F\r\na=ssrc:1231573672 msid:UuELRAP9osB2u2DSvQTl1a6AlL5XRcpV78yV 6aae665a-d1f6-4691-8f94-d0e3bcc72d59\r\na=ssrc:1231573672 mslabel:UuELRAP9osB2u2DSvQTl1a6AlL5XRcpV78yV\r\na=ssrc:1231573672 label:6aae665a-d1f6-4691-8f94-d0e3bcc72d59\r\n";
        try
        {
            // Attempt a call
            interop.doCall("6809", sdp);
        }
        catch (IOException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        // Keep things running
        while (true)
        {
            try
            {
                Thread.sleep(1000);
            }
            catch (InterruptedException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
