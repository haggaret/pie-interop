package com.magor.pie.interop;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InteropEndpoint
{
    private static Logger log = LoggerFactory.getLogger(InteropEndpoint.class);

    public enum MsgType
    {
     LOGGED_IN,
     IDLE,
     INCOMING_CALL,
     CONNECTED,
     ANSWER,
     OUTGOING_CALL,
     CANCEL,
     CALL_ENDED,
     ERROR
    }

    private WebsocketClientEndpoint wsc;
    private String sessId;
    private MessageHandler interopMessageHandler;
    private String outgoingSdp;
    private String incomingSdp;
    private String callId;
    private DialogParams outgoingDialogParams;
    private WebsocketClientEndpoint.MessageHandler onWSMessage;

    public InteropEndpoint(String uri)
    {
        createWebsocketToServer(uri);
    }

    public String getOutgoingSdp()
    {
        return outgoingSdp;
    }

    public void setOutgoingSdp(String outgoingSdp)
    {
        this.outgoingSdp = outgoingSdp;
    }

    public String getIncomingSdp()
    {
        return incomingSdp;
    }

    public void setIncomingSdp(String incomingSdp)
    {
        this.incomingSdp = incomingSdp;
    }

    public void createWebsocketToServer(String uri)
    {
        log.info("Starting connection to FreeSWITCH mod_verto at URI: " + uri);

        onWSMessage = new WebsocketClientEndpoint.MessageHandler()
        {
            @Override
            public void handleMessage(JsonRpcMessage message)
            {
                // TODO Auto-generated method stub
                handleWebsocketMessage(message);
            }
        };

        this.wsc = new WebsocketClientEndpoint();

        // add listener
        wsc.addDefaultMessageHandler(onWSMessage);

        try
        {
            wsc.connect(uri);

            JsonRpcMsgParams params = new JsonRpcMsgParams();
            sessId = UUID.randomUUID().toString();
            params.setSessid(sessId);

            log.info("Attempting to Login");
            try
            {
                wsc.sendMessage("login", params, null);
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        catch (URISyntaxException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void handleWebsocketMessage(JsonRpcMessage message)
    {
        if (message != null)
        {
            log.debug("Got: " + message.toString());

            try
            {
                if (message.getError() != null)
                {
                    handleErrorMessage(message);
                    return;
                }
                if (message.getResult() != null)
                {
                    handleResultMessage(message);
                    return;
                }
                if (message.getMethod().equalsIgnoreCase("verto.invite"))
                {
                    handleInvite(message);
                    return;
                }
                if (message.getMethod().equalsIgnoreCase("verto.answer"))
                {
                    handleAnswer(message);
                    return;
                }
                if (message.getMethod().equalsIgnoreCase("verto.display"))
                {
                    handleDisplay(message);
                    return;
                }
                if (message.getMethod().equalsIgnoreCase("verto.bye"))
                {
                    handleBye(message);
                    return;
                }
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public void handleResponseMessage(JsonRpcMessage message)
    {
        log.info("This is handling a response for a message with Id: " + String.valueOf(message.getId()));
    }

    public void handleErrorMessage(JsonRpcMessage rpcMsg) throws IOException
    {
        log.info("Got the following error:");
        log.info("   Code: " + String.valueOf(rpcMsg.getError().getCode()));
        log.info("   Message: " + rpcMsg.getError().getMessage());

        if (rpcMsg.getError().getCode() == -32000)
        {
            if (this.interopMessageHandler != null)
            {
                this.interopMessageHandler.handleMessage(MsgType.ERROR, rpcMsg);
            }
            handleAuthRequiredMessage(rpcMsg);
        }
    }

    public void handleAuthRequiredMessage(JsonRpcMessage rpcMsg) throws IOException
    {
        JsonRpcMsgParams params = new JsonRpcMsgParams();
        params.setSessid(sessId);
        params.setLogin("1000@verto.magor.local");
        params.setPasswd("1234");
        params.setIceServers("false");

        log.info("Performing login");
        wsc.sendMessage("login", params, null);
    }

    public void handleResultMessage(JsonRpcMessage rpcMsg) throws IOException
    {
        log.info("Got the following result:");
        if (rpcMsg.getResult().getMessage() != null)
        {
            log.info("   Message: " + String.valueOf(rpcMsg.getResult().getMessage()));
            if ("logged in".equalsIgnoreCase(rpcMsg.getResult().getMessage()))
            {
                if (this.interopMessageHandler != null)
                {
                    this.interopMessageHandler.handleMessage(MsgType.LOGGED_IN, rpcMsg);
                }
            }
        }
        if (rpcMsg.getResult().getSessid() != null)
        {
            log.info("   Session ID: " + rpcMsg.getResult().getSessid());
        }
        if (rpcMsg.getResult().getCallID() != null)
        {
            log.info("   Call ID: " + rpcMsg.getResult().getCallID());
        }
    }

    private DialogParams generateDialogParams(JsonRpcMsgParams params)
    {
        VideoParams videoParams = new VideoParams();
        videoParams.setMinWidth("1280");
        videoParams.setMinHeight("720");
        videoParams.setMinFrameRate(30);
        DialogParams dialogParams = new DialogParams();
        dialogParams.setUseVideo(true);
        dialogParams.setUseStereo(true);
        dialogParams.setScreenShare(false);
        dialogParams.setUseMic("any");
        dialogParams.setUseSpeak("any");
        dialogParams.setTag("webcam");
        dialogParams.setLogin("1000@verto.magor.local");
        dialogParams.setVideoParams(videoParams);
        dialogParams.setCallID(params.getCallID());
        dialogParams.setCallerIdName(params.getCallerIdName());
        dialogParams.setCallerIdNumber(params.getCallerIdNumber());
        dialogParams.setDisplayDirection(params.getDisplayDirection());
        dialogParams.setWantVideo(true);
        dialogParams.setRemoteCallerIdName(params.getCallerIdName());
        dialogParams.setRemoteCallerIdNumber(params.getCallerIdNumber());
        return dialogParams;
    }

    public void handleInvite(JsonRpcMessage rpcMsg)
    {
        log.info("Invite Controller");
        if (rpcMsg.getParams().getDisplayDirection().equalsIgnoreCase("outbound"))
        {
            log.info("   INCOMING call");
            log.info("   SDP:\n" + rpcMsg.getParams().getSdp());

            if (this.interopMessageHandler != null)
            {
                this.interopMessageHandler.handleMessage(MsgType.INCOMING_CALL, rpcMsg);
            }
        }
    }

    public void handleAnswer(JsonRpcMessage rpcMsg)
    {
        log.info("Answer Controller");
        log.info("   SDP:\n" + rpcMsg.getParams().getSdp());

        try
        {
            // send the verto.answer result back
            wsc.sendResultMessage("verto.answer", rpcMsg.getId(), null);
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        if (this.interopMessageHandler != null)
        {
            this.interopMessageHandler.handleMessage(MsgType.ANSWER, rpcMsg);
        }
    }

    public void handleDisplay(JsonRpcMessage rpcMsg)
    {
        log.info("Display Controller");

        try
        {
            // send the verto.answer result back
            wsc.sendResultMessage("verto.display", rpcMsg.getId(), null);
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        if (this.interopMessageHandler != null)
        {
            this.interopMessageHandler.handleMessage(MsgType.ANSWER, rpcMsg);
        }
    }

    public void doAnswer(JsonRpcMsgParams rpcMsgParams, String sdp) throws IOException
    {
        JsonRpcMsgParams params = new JsonRpcMsgParams();
        params.setSdp(sdp);
        params.setDialogParams(generateDialogParams(rpcMsgParams));
        params.setSessid(sessId);

        log.info("Attempting to answer call");
        wsc.sendMessage("verto.answer", params, null);
    }

    private DialogParams generateOutgoingDialogParams(String destinationNumber)
    {
        VideoParams videoParams = new VideoParams();
        videoParams.setMinWidth("1280");
        videoParams.setMinHeight("720");
        videoParams.setMinFrameRate(30);
        if (outgoingDialogParams == null)
        {
            outgoingDialogParams = new DialogParams();
        }
        outgoingDialogParams.setUseVideo(true);
        outgoingDialogParams.setUseStereo(false);
        outgoingDialogParams.setScreenShare(false);
        outgoingDialogParams.setUseMic("any");
        outgoingDialogParams.setUseSpeak("any");
        outgoingDialogParams.setTag("webcam");
        outgoingDialogParams.setLogin("1000@verto.magor.local");
        outgoingDialogParams.setVideoParams(videoParams);
        outgoingDialogParams.setDestinationNumber(destinationNumber);
        outgoingDialogParams.setCallID(callId);
        outgoingDialogParams.setCallerIdName("FreeSWITCH User");
        outgoingDialogParams.setCallerIdNumber("1000");
        outgoingDialogParams.setRemoteCallerIdName("Outbound Call");
        outgoingDialogParams.setRemoteCallerIdNumber(destinationNumber);
        return outgoingDialogParams;
    }

    public void saveSdp(String sdp)
    {
        outgoingSdp = sdp;
    }

    public void appendIceCandidate(String candidate)
    {
        outgoingSdp += candidate;
    }

    public void doCall(String sipUri, String sdp) throws IOException
    {
        JsonRpcMsgParams params = new JsonRpcMsgParams();
        params.setSdp(sdp);
        callId = UUID.randomUUID().toString();
        params.setDialogParams(generateOutgoingDialogParams(sipUri));
        params.setSessid(sessId);

        log.info("Attempting to MAKE call");
        wsc.sendMessage("verto.invite", params, null);
    }

    public void doEnd() throws IOException
    {
        JsonRpcMsgParams params = new JsonRpcMsgParams();
        params.setDialogParams(outgoingDialogParams);
        params.setSessid(sessId);

        log.info("Attempting to end call");
        wsc.sendMessage("verto.bye", params, null);
    }

    public void handleBye(JsonRpcMessage rpcMsg)
    {
        log.info("Call Ended");
        log.info("   callId:    " + rpcMsg.getParams().getCallID());
        log.info("   causeCode: " + String.valueOf(rpcMsg.getParams().getCauseCode()));
        log.info("   cause:     " + rpcMsg.getParams().getCause());

        if (this.interopMessageHandler != null)
        {
            this.interopMessageHandler.handleMessage(MsgType.CANCEL, rpcMsg);
        }
    }

    /**
     * register message handler
     *
     * @param msgHandler
     */
    public void addMessageHandler(MessageHandler msgHandler)
    {
        log.info("Adding message handler for InteropEndpoint");
        this.interopMessageHandler = msgHandler;
    }

    /**
     * Message handler.
     *
     */
    public interface MessageHandler
    {
        public void handleMessage(MsgType type, JsonRpcMessage message);
    }
}
