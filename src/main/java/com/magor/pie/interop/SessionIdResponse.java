package com.magor.pie.interop;

import java.io.Serializable;

public class SessionIdResponse implements Serializable
{
    private static final long serialVersionUID = 1L;

    public static class Data implements Serializable
    {
        private static final long serialVersionUID = 1L;

        private long id;

        public Data()
        {

        }

        public long getId()
        {
            return id;
        }

        public void setId(long id)
        {
            this.id = id;
        }

        @Override
        public String toString()
        {
            return String.format("Data [id=%s]", id);
        }
    }

    private String janus;
    private String transaction;
    private Data data;

    public SessionIdResponse()
    {

    }

    public String getJanus()
    {
        return janus;
    }

    public void setJanus(String janus)
    {
        this.janus = janus;
    }

    public String getTransaction()
    {
        return transaction;
    }

    public void setTransaction(String transaction)
    {
        this.transaction = transaction;
    }

    public Data getData()
    {
        return data;
    }

    public void setData(Data data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return String.format("SessionPluginHandle [janus=%s, transaction=%s, data=%s]", janus, transaction, data);
    }
}
