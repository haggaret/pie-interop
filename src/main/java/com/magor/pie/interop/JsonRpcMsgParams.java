package com.magor.pie.interop;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class JsonRpcMsgParams implements Serializable
{
    public static class LoginParams implements Serializable
    {
        private static final long serialVersionUID = 1L;

        private String params;

        public LoginParams()
        {

        }

        public String getParams()
        {
            return params;
        }

        public void setParams(String params)
        {
            this.params = params;
        }

        @Override
        public String toString()
        {
            return String.format("LoginParams [params=%s]", params);
        }
    }

    public static class UserVariables implements Serializable
    {
        private static final long serialVersionUID = 1L;

        private String variables;

        public UserVariables()
        {

        }

        public String getVariables()
        {
            return variables;
        }

        public void setVariables(String variables)
        {
            this.variables = variables;
        }

        @Override
        public String toString()
        {
            return String.format("UserVariables [variables=%s]", variables);
        }
    }

    private static final long serialVersionUID = 1L;
    private String sessid;
    private String login;
    private String passwd;
    private String callID;
    private Integer causeCode;
    private String cause;
    private String sdp;
    @JsonProperty("caller_id_name")
    private String callerIdName;
    @JsonProperty("caller_id_number")
    private String callerIdNumber;
    @JsonProperty("callee_id_name")
    private String calleeIdName;
    @JsonProperty("callee_id_number")
    private String calleeIdNumber;
    @JsonProperty("display_direction")
    private String displayDirection;
    Boolean useStereo;
    Boolean screenShare;
    String useMic;
    String useSpeak;
    String iceServers;
    String tag;
    String localTag;
    private LoginParams loginParams;
    private UserVariables userVariables;
    private VideoParams videoParams;
    private DialogParams dialogParams;
    Boolean wantVideo;
    @JsonProperty("remote_caller_id_name")
    private String remoteCallerIdName;
    @JsonProperty("remote_caller_id_number")
    private String remoteCallerIdNumber;
    @JsonProperty("display_name")
    private String displayName;
    @JsonProperty("display_number")
    private String displayNumber;

    public JsonRpcMsgParams()
    {

    }

    public String getSessid()
    {
        return sessid;
    }

    public void setSessid(String sessid)
    {
        this.sessid = sessid;
    }

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getPasswd()
    {
        return passwd;
    }

    public void setPasswd(String passwd)
    {
        this.passwd = passwd;
    }

    public String getCallID()
    {
        return callID;
    }

    public void setCallID(String callID)
    {
        this.callID = callID;
    }

    public Integer getCauseCode()
    {
        return causeCode;
    }

    public void setCauseCode(Integer causeCode)
    {
        this.causeCode = causeCode;
    }

    public String getCause()
    {
        return cause;
    }

    public void setCause(String cause)
    {
        this.cause = cause;
    }

    public String getSdp()
    {
        return sdp;
    }

    public void setSdp(String sdp)
    {
        this.sdp = sdp;
    }

    public String getCallerIdName()
    {
        return callerIdName;
    }

    public void setCallerIdName(String callerIdName)
    {
        this.callerIdName = callerIdName;
    }

    public String getCallerIdNumber()
    {
        return callerIdNumber;
    }

    public void setCallerIdNumber(String callerIdNumber)
    {
        this.callerIdNumber = callerIdNumber;
    }

    public String getCalleeIdName()
    {
        return calleeIdName;
    }

    public void setCalleeIdName(String calleeIdName)
    {
        this.calleeIdName = calleeIdName;
    }

    public String getCalleeIdNumber()
    {
        return calleeIdNumber;
    }

    public void setCalleeIdNumber(String calleeIdNumber)
    {
        this.calleeIdNumber = calleeIdNumber;
    }

    public String getDisplayDirection()
    {
        return displayDirection;
    }

    public void setDisplayDirection(String displayDirection)
    {
        this.displayDirection = displayDirection;
    }

    public String getUseMic()
    {
        return useMic;
    }

    public void setUseMic(String useMic)
    {
        this.useMic = useMic;
    }

    public String getUseSpeak()
    {
        return useSpeak;
    }

    public void setUseSpeak(String useSpeak)
    {
        this.useSpeak = useSpeak;
    }

    public String getIceServers()
    {
        return iceServers;
    }

    public void setIceServers(String iceServers)
    {
        this.iceServers = iceServers;
    }

    public String getTag()
    {
        return tag;
    }

    public void setTag(String tag)
    {
        this.tag = tag;
    }

    public String getLocalTag()
    {
        return localTag;
    }

    public void setLocalTag(String localTag)
    {
        this.localTag = localTag;
    }

    public VideoParams getVideoParams()
    {
        return videoParams;
    }

    public void setVideoParams(VideoParams videoParams)
    {
        this.videoParams = videoParams;
    }

    public String getRemoteCallerIdName()
    {
        return remoteCallerIdName;
    }

    public void setRemoteCallerIdName(String remoteCallerIdName)
    {
        this.remoteCallerIdName = remoteCallerIdName;
    }

    public String getRemoteCallerIdNumber()
    {
        return remoteCallerIdNumber;
    }

    public void setRemoteCallerIdNumber(String remoteCallerIdNumber)
    {
        this.remoteCallerIdNumber = remoteCallerIdNumber;
    }

    public LoginParams getLoginParams()
    {
        return loginParams;
    }

    public void setLoginParams(LoginParams loginParams)
    {
        this.loginParams = loginParams;
    }

    public DialogParams getDialogParams()
    {
        return dialogParams;
    }

    public void setDialogParams(DialogParams dialogParams)
    {
        this.dialogParams = dialogParams;
    }

    public UserVariables getUserVariables()
    {
        return userVariables;
    }

    public void setUserVariables(UserVariables userVariables)
    {
        this.userVariables = userVariables;
    }

    public Boolean getUseStereo()
    {
        return useStereo;
    }

    public void setUseStereo(Boolean useStereo)
    {
        this.useStereo = useStereo;
    }

    public Boolean getScreenShare()
    {
        return screenShare;
    }

    public void setScreenShare(Boolean screenShare)
    {
        this.screenShare = screenShare;
    }

    public Boolean getWantVideo()
    {
        return wantVideo;
    }

    public void setWantVideo(Boolean wantVideo)
    {
        this.wantVideo = wantVideo;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public String getDisplayNumber()
    {
        return displayNumber;
    }

    public void setDisplayNumber(String displayNumber)
    {
        this.displayNumber = displayNumber;
    }

    @Override
    public String toString()
    {
        return String.format("JsonRpcMsgParams [sessid=%s, login=%s, passwd=%s, callID=%s, causeCode=%s, cause=%s, sdp=%s, callerIdName=%s, callerIdNumber=%s, calleeIdName=%s, calleeIdNumber=%s, displayDirection=%s, useStereo=%s, screenShare=%s, useMic=%s, useSpeak=%s, iceServers=%s, tag=%s, localTag=%s, loginParams=%s, userVariables=%s, videoParams=%s, dialogParams=%s, wantVideo=%s, remoteCallerIdName=%s, remoteCallerIdNumber=%s, displayName=%s, displayNumber=%s]",
                             sessid, login, passwd, callID, causeCode, cause, sdp, callerIdName, callerIdNumber, calleeIdName,
                             calleeIdNumber, displayDirection, useStereo, screenShare, useMic, useSpeak, iceServers, tag, localTag,
                             loginParams, userVariables, videoParams, dialogParams, wantVideo, remoteCallerIdName, remoteCallerIdNumber,
                             displayName, displayNumber);
    }
}
