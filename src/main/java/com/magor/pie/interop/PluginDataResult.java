package com.magor.pie.interop;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

//@JsonIgnoreProperties(ignoreUnknown = true)
public class PluginDataResult implements Serializable
{
    private static final long serialVersionUID = 1L;

    private String username;
    private String event;
    private String displayname;
    @JsonProperty("register_sent")
    private boolean registerSent;
    private String reason;
    private int code;

    public PluginDataResult()
    {

    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getEvent()
    {
        return event;
    }

    public void setEvent(String event)
    {
        this.event = event;
    }

    public String getDisplayname()
    {
        return displayname;
    }

    public void setDisplayname(String displayname)
    {
        this.displayname = displayname;
    }

    public boolean isRegisterSent()
    {
        return registerSent;
    }

    public void setRegisterSent(boolean registerSent)
    {
        this.registerSent = registerSent;
    }

    public String getReason()
    {
        return reason;
    }

    public void setReason(String reason)
    {
        this.reason = reason;
    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return String.format("PluginDataResult [username=%s, event=%s, displayname=%s, registerSent=%s, reason=%s, code=%s]", username,
                             event, displayname, registerSent, reason, code);
    }
}
