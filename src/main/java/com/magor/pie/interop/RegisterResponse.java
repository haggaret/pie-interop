package com.magor.pie.interop;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

//@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterResponse implements Serializable
{
    private static final long serialVersionUID = 1L;

    private String janus;
    private String transaction;
    @JsonProperty("session_id")
    private long sessionId;

    public RegisterResponse()
    {

    }

    public String getJanus()
    {
        return janus;
    }

    public void setJanus(String janus)
    {
        this.janus = janus;
    }

    public String getTransaction()
    {
        return transaction;
    }

    public void setTransaction(String transaction)
    {
        this.transaction = transaction;
    }

    public long getsessionId()
    {
        return sessionId;
    }

    public void setSessionId(long sessionId)
    {
        this.sessionId = sessionId;
    }

    @Override
    public String toString()
    {
        return String.format("RegisterResponse [janus=%s, transaction=%s, sessionId=%s]", janus, transaction, sessionId);
    }
}
