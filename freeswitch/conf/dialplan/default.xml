<?xml version="1.0" encoding="utf-8"?>
<!--
    NOTICE:
    
    This context is usually accessed via authenticated callers on the sip profile on port 5060 
    or transfered callers from the public context which arrived via the sip profile on port 5080.
    
    Authenticated users will use the user_context variable on the user to determine what context
    they can access.  You can also add a user in the directory with the cidr= attribute acl.conf.xml
    will build the domains ACL using this value.
-->
<!-- http://wiki.freeswitch.org/wiki/Dialplan_XML -->
<include>
  <context name="default">

    <extension name="unloop">
      <condition field="${unroll_loops}" expression="^true$"/>
      <condition field="${sip_looped_call}" expression="^true$">
    <action application="deflect" data="${destination_number}"/>
      </condition>
    </extension>

    <extension name="global-intercept">
      <condition field="destination_number" expression="^886$">
    <action application="answer"/>
    <action application="intercept" data="${hash(select/${domain_name}-last_dial_ext/global)}"/>
    <action application="sleep" data="2000"/>
      </condition>
    </extension>

    <extension name="global" continue="true">
      <condition field="${call_debug}" expression="^true$" break="never">
    <action application="info"/>
      </condition>

<!--
      <condition field="${default_password}" expression="^1234$" break="never">
    <action application="log" data="CRIT WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING "/>
    <action application="log" data="CRIT Open $${conf_dir}/vars.xml and change the default_password."/>
    <action application="log" data="CRIT Once changed type 'reloadxml' at the console."/>
    <action application="log" data="CRIT WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING "/>
    <action application="sleep" data="10000"/>
      </condition>
-->
      <!--
      This is an example of how to auto detect if telephone-event is missing and activate inband detection 
      -->
      <!--
      <condition field="${switch_r_sdp}" expression="a=rtpmap:(\d+)\stelephone-event/8000" break="never">
    <action application="set" data="rtp_payload_number=$1"/>
    <anti-action application="start_dtmf"/>
      </condition>
      -->
      <condition field="${rtp_has_crypto}" expression="^($${rtp_sdes_suites})$" break="never">
    <action application="set" data="rtp_secure_media=true"/>
    <!-- Offer SRTP on outbound legs if we have it on inbound. -->
    <!-- <action application="export" data="rtp_secure_media=true"/> -->
      </condition>

      <!--
     Since we have inbound-late-negotation on by default now the
     above behavior isn't the same so you have to do one extra step.
    -->
      <condition field="${endpoint_disposition}" expression="^(DELAYED NEGOTIATION)"/>
      <condition field="${switch_r_sdp}" expression="(AES_CM_128_HMAC_SHA1_32|AES_CM_128_HMAC_SHA1_80)" break="never">
    <action application="set" data="rtp_secure_media=true"/>
    <!-- Offer SRTP on outbound legs if we have it on inbound. -->
    <!-- <action application="export" data="rtp_secure_media=true"/> -->
      </condition>


      <condition>
    <action application="hash" data="insert/${domain_name}-spymap/${caller_id_number}/${uuid}"/>
    <action application="hash" data="insert/${domain_name}-last_dial/${caller_id_number}/${destination_number}"/>
    <action application="hash" data="insert/${domain_name}-last_dial/global/${uuid}"/>
    <action application="export" data="RFC2822_DATE=${strftime(%a, %d %b %Y %T %z)}"/>
      </condition>
    </extension>

    <!-- If sip_req_host is not a local domain then this has to be an external sip uri -->
    <!--
    <extension name="external_sip_uri" continue="true">
      <condition field="source" expression="mod_sofia"/>
      <condition field="${outside_call}" expression="^$"/>
      <condition field="${domain_exists(${sip_req_host})}" expression="true">
    <anti-action application="bridge" data="sofia/${use_profile}/${sip_to_uri}"/>
      </condition>
    </extension>
    -->

    <!-- 
     This extension is used by mod_portaudio so you can pa call sip:someone@example.com
     mod_portaudio will pass the entire string to the dialplan for routing.
    -->
    <extension name="sip_uri">
      <condition field="destination_number" expression="^sip:(.*)$">
    <action application="bridge" data="sofia/${use_profile}/$1"/>
      </condition>
    </extension>

    <!-- dial the FreeSWITCH conference via SIP-->
    <extension name="freeswitch_public_conf_via_sip">
      <condition field="destination_number" expression="^9(888|8888|1616|3232)$">
    <action application="export" data="hold_music=silence"/>
    <!-- 
         This will take the SAS from the b-leg and send it to the display on the a-leg phone.
         Known working with Polycom and Snom maybe others.
    -->
    <!--
    <action application="set" data="exec_after_bridge_app=${sched_api(+4 zrtp expand uuid_display ${uuid} \${uuid_getvar(\${uuid_getvar(${uuid} signal_bond)} zrtp_sas1_string )}  \${uuid_getvar(\${uuid_getvar(${uuid} signal_bond)} zrtp_sas2_string )} )}"/>
    <action application="export" data="nolocal:zrtp_secure_media=true"/>
    -->
    <action application="bridge" data="sofia/${use_profile}/$1@conference.freeswitch.org"/>
      </condition>
    </extension>

    <extension name="delay_echo">
      <condition field="destination_number" expression="^9195$">
    <action application="answer"/>
    <action application="delay_echo" data="5000"/>
      </condition>
    </extension>

    <extension name="echo">
      <condition field="destination_number" expression="^9196$">
    <action application="answer"/>
    <action application="echo"/>
      </condition>
    </extension>

    <extension name="milliwatt">
      <condition field="destination_number" expression="^9197$">
    <action application="answer"/>
    <action application="playback" data="{loops=-1}tone_stream://%(251,0,1004)"/>
      </condition>
    </extension>

    <extension name="tone_stream">
      <condition field="destination_number" expression="^9198$">
    <action application="answer"/>
    <action application="playback" data="{loops=10}tone_stream://path=${conf_dir}/tetris.ttml"/>
      </condition>
    </extension>

    <extension name="laugh break">
      <condition field="destination_number" expression="^9386$">
        <action application="answer"/>
        <action application="sleep" data="1500"/>
        <action application="playback" data="phrase:funny_prompts"/>
        <action application="hangup"/>
      </condition>
    </extension>

    <!--
    You can place files in the default directory to get included.
    -->
    <X-PRE-PROCESS cmd="include" data="default/*.xml"/>
    
    <!--
    <extension name="refer">
      <condition field="${sip_refer_to}">
    <expression><![CDATA[<sip:${destination_number}@${domain_name}>]]></expression>
      </condition>
      <condition field="${sip_refer_to}">
    <expression><![CDATA[<sip:(.*)@(.*)>]]></expression>
    <action application="set" data="refer_user=$1"/>
    <action application="set" data="refer_domain=$2"/>
    <action application="info"/>
    <action application="bridge" data="sofia/${use_profile}/${refer_user}@${refer_domain}"/>
      </condition>
    </extension>
    -->
    <!--
    This is an example of how to override the RURI on an outgoing invite to a registered contact.
    -->
    <!--
    <extension name="ruri">
      <condition field="destination_number" expression="^ruri$">
    <action application="bridge" data="sofia/${ruri_profile}/${ruri_user}${regex(${sofia_contact(${ruri_contact})}|^[^\@]+(.*)|%1)}"/>
      </condition>
    </extension>
    
    <extension name="7004">
      <condition field="destination_number" expression="^7004$">
    <action application="set" data="ruri_profile=default"/>
    <action application="set" data="ruri_user=2000"/>
    <action application="set" data="ruri_contact=1001@${domain_name}"/>
    <action application="execute_extension" data="ruri"/>
      </condition>
    </extension>
    -->

    <extension name="verto_call" continue="true">
        <condition field="caller-source" expression="mod_verto">
            <action application="log" data="INFO ***** Verto WebRTC Call ***** "/>
        </condition>
    </extension>

    <extension name="verto_to_bucket" continue="true">
        <condition field="destination_number" expression="^6887$">
            <action application="answer"/> 
            <action application="playback" data="silence_stream://2000"/>
            <action application="set" data="effective_caller_id_number=1234567890"/>
            <action application="set" data="effective_caller_id_name=webrtccall"/>
            <action application="log" data="INFO ***** Outbound Verto WebRTC Call ***** "/>
            <action application="bridge" data="sofia/external/6887@192.168.217.88"/>
        </condition>
    </extension>

    <extension name="verto_to_khag" continue="true">
        <condition field="destination_number" expression="^6809$">
            <action application="answer"/> 
            <action application="playback" data="silence_stream://2000"/>
            <action application="set" data="effective_caller_id_number=1234567890"/>
            <action application="set" data="effective_caller_id_name=webrtccall"/>
            <action application="log" data="INFO ***** Outbound Verto WebRTC Call ***** "/>
            <action application="bridge" data="sofia/external/6809@192.168.217.81"/>
        </condition>
    </extension>

    <extension name="verto_to_t1000" continue="true">
        <condition field="destination_number" expression="^6818$">
            <action application="answer"/> 
            <action application="playback" data="silence_stream://2000"/>
            <action application="set" data="effective_caller_id_number=1234567890"/>
            <action application="set" data="effective_caller_id_name=webrtccall"/>
            <action application="log" data="INFO ***** Outbound Verto WebRTC Call ***** "/>
            <action application="bridge" data="sofia/external/1000@192.168.217.18"/>
        </condition>
    </extension>

    <extension name="inbound_verto_to_1000" continue="true">
        <condition field="destination_number" expression="^1000">
            <action application="bridge" data="${verto_contact 1000@${domain_name}}"/> 
        </condition>
    </extension>

<!--
    <extension name="itsp_send_call" continue="true">
        <condition field="destination_number" expression="^(\d+)$">
-->
<!--            <action application="answer"/> -->
<!--            <action application="playback" data="silence_stream://2000"/> -->
<!--
            <action application="set" data="effective_caller_id_number=1234567890"/>
            <action application="set" data="effective_caller_id_name=webrtccall"/>
            <action application="log" data="INFO ***** Outbound Verto WebRTC Call ***** "/>
            <action application="set" data="bypass_media=true"/>
            <action application="bridge" data="sofia/external/6887@192.168.217.88"/>
        </condition>
    </extension>
-->

    <extension name="enum">
      <condition field="${module_exists(mod_enum)}" expression="true"/>
      <condition field="destination_number" expression="^(.*)$">
    <action application="transfer" data="$1 enum"/>
      </condition>
    </extension>

  </context>
</include>
